**About**

This is my technology/productivity/learning blog. I enjoy learning new things
and writing about them. This is a specialized blog that has almost
no readers and appeals to pretty much nobody. I'm aware that it uses 
plain styling and no Javascript. That's the way it's supposed to be. The 
fact that the rest of the world does html wrong is not a reason for me to join them.

**Posts**

- [Web Fonts, an Unsolved Problem](web-fonts.md)
- [The Relevance of Contexts](contexts.md)
- [Low cost project management](proj-man.md)
- [Links: Free data science book, Digital Ocean alternatives](links-nov-21.md)
- [Links: Run your own mail server, history of usenet](links-mail-usenet.md)
- [I'm blogging again](intro.md)
