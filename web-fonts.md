# Web Fonts, an Unsolved Problem

I recently came to a realization that web fonts are *not* a solved problem. I had been under the impression that Google Fonts had indeed given the content creator full control over the display of text. Not so, I have learned.[^1] There are two problems:

- It can take a very long time for these fonts to load if you have a bad connection, and I have a horrible internet provider. Making matters worse, there doesn't seem to be any way to ensure browsers will cache them. I learned the hard way, by repeatedly making changes to an html page and reloading with every change. That did not end well.
- Even if you have a spectacular internet connection, there's no guarantee that the fonts will display well. After waiting forever for the font to load, it didn't display with anything close to the quality that it did natively. Some research indicated that there's no way to guarantee a font will display the way you want.

In other words, Google Fonts don't solve anything. They merely improve the situation a bit.

I've decided to move back to "safe web fonts". I've always been concerned with the idea of having to download one or more fonts to display a page. Using a native font reduces bandwidth and improves rendering speed.

Here's one possible font stack:

content: Roboto, Arial, sans-serif

headings: Ubuntu, Trebuchet MS, sans-serif

code: Ubuntu Mono, courier new, monospace

Georgia is also a good choice for content, but I'm personally not a fan of serif fonts.

[^1]: I don't do much web development. This information is likely common knowledge among the web development savvy.
