files := $(patsubst %.md,%,$(wildcard *.md))

all:
	for f in $(files); do \
		pandoc -s -o $$f.html --template=template.html --mathjax=https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML $$f.md; \
	done

one:
	pandoc -s $(name).md -o $(name).html --template=template.html --mathjax=https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.1/MathJax.js?config=TeX-MML-AM_CHTML
