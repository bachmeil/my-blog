# Some low-cost project management solutions

If you're looking for a way to manage your projects, here are a few
you might want to consider:

- Basecamp Personal. Three projects for free.
- Notion. Free for anyone with a university email address.
- Nozbe. Provides everyone with one free project. You can use it to organize your stuff even if you don't use it for all your projects.
- Trello. Started the recent Kanban craze with their free product.
- Asana. The free plan will work perfectly well if it's just you.
- Freedcamp. Really good free plan, not that expensive to upgrade for more features.
