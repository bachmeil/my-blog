# Calling grep from D

Sometimes it's convenient to call grep or another GNU utility from inside a D program. If you're working with a file, there's really no difficulty:

```
import std.process;
string cmd = `grep 'butter' file.txt`;
string butterLines = cmd.executeShell.output;
```

`butterLines` is a string holding all lines in your file containing "butter". Pretty simple.

But what if you have a string in your D program and you want to use grep on it? If you were at the command line, you could do this:

```
echo 'a block of text
> even including line feeds
> that you want to send to grep' | grep 'line feeds'
```

The output is:

```
even including line feeds
```

You could replicate the same behavior in D, but have the output stored in a D variable, with this code:

```
string txt = "a block of text
even including line feeds
that you want to send to grep";
string cmd = `echo '` ~ txt ~ `' | grep 'line feeds'`;
string output = cmd.executeShell.output;
```

You're not limited to just using grep, however. You can do the same thing with many GNU utilities, as well as command line programs like Pandoc.

Where you will run into trouble is if you have an embedded single quote `'` character in the string. If `txt` instead looked like this:

```
string txt = "a 'block of text'
even including line feeds
that you want to send to grep";
```

you wouldn't get the result you expected, because

```
string cmd = `echo '` ~ txt ~ `' | grep 'line feeds'`;
```

would translate into

```
string cmd = `echo 'a 'block of text'
even including line feeds
that you want to send to grep' | grep 'line feeds'`;
```

That would result in an error because the argument to `echo` would only have two characters: 'a '. The first single quote inside `txt` would be the end of the argument.

There's an easy way to fix this, using the solution provided at
https://stackoverflow.com/questions/1250079/how-to-escape-single-quotes-within-single-quoted-strings

```
import std.string;
string newtxt = txt.replace("'", `'"'"'`);
string cmd = `echo '` ~ newtxt ~ `' | grep 'line feeds'`;
```

Since the text in single quotes in a shell command is passed on verbatim (unlike double quotes), the only escaping you need is to prevent input from being interpreted as a closing quote. The replacement above does that. The explanation can be found in the accepted answer at the link above, but you can understand the mechanism by typing this into the terminal:

```
echo 'this'' is ''a word'
```

for which the output is

```
this is a word
```


