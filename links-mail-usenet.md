# HN Links, Nov 19 2019

[How To Run Your Own Mail Server](https://www.c0ffee.net/blog/mail-server-guide/)

A guide to self-hosting your email on FreeBSD using Postfix, Dovecot, Rspamd, and LDAP.

[The Early History of Usenet, Part I: The Technological Setting](https://www.cs.columbia.edu/~smb/blog/2019-11/2019-11-14a.html)

Usenet—Netnews—was conceived almost exactly 40 years ago this month. To understand where it came from and why certain decisions were made the way they were, it's important to understand the technological constraints of the time.

---

As an aside, I like the fact that these pages are *readable*. They're just plain text without a pile of JavaScript and styling. That's the way websites are supposed to be.
