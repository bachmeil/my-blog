> Wondering what folks in this community are doing in terms of contexts and delineating this part of their gtd system? Has it evolved over time? What factors do you consider most important to be aware of?

Contexts are still relevant for me:

- work
- home
- desktop computer
- outside
- evening
- weekday
- weekend

There are a few others, but you get the idea. Yes, contexts are still relevant for me.

Maybe it will help to explain *why* we need contexts. There are some things you can only do in a certain state of the world. It's bad to be looking at things you can't do at that time. You don't want to have to figure out which of your next actions you are able to do at a point in time. Once you think of that, you'll have to figure out which actions are the best to do out of the ones that are possible. You're not doing at that time, and you're burning through your brain power trying to figure out what to do.
