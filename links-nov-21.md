# Links for November 21 2019

[Introduction to Data Science: Data Analysis and Prediction Algorithms with R](https://rafalab.github.io/dsbook/)

## Digital Ocean Competitors

Some alternatives to consider:

[vultr](https://www.vultr.com/products/cloud-compute/) 10 GB, 512 MB, 0.5 TB, $2.50/month  
[Linode](https://www.linode.com/pricing/) 25 GB, 1 GB, 1 TB, $5/month  
[Scaleway](https://www.scaleway.com/en/pricing/) 2 CPUs, 20 GB, 2 GB, 3 euros/month  
[Amazon Lightsail](https://aws.amazon.com/lightsail/pricing/?opdp1=pricing) 20 GB, 512 MB, 1 TB, $3.50/month  
[Exoscale](https://www.exoscale.com/pricing/) 10 GB, 512 MB, 4.54 euros/month  

Storage:

[Hetzner](https://www.hetzner.com/storage/storage-share) 100 GB, 3.45 euros/month  

Static hosting:

[ZEIT](https://zeit.co/pricing) Free custom domain with https, static website files
**Note:** This site uses ZEIT.  
[Netlify](https://www.netlify.com/) Free static website hosting
